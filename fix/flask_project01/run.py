# 「__init__.py」で宣言した変数appを取得
from practice import app

# アプリケーションを起動
if __name__ == '__main__':
  app.run(debug=True)