# flaskのインポート
from flask import Flask

#Flaskのアプリケーション本体を変数appに代入
app = Flask(__name__)

# 「config.py」を設定ファイルをとして扱う
app.config.from_object('practice.config')

# ビューをインポート
import practice.views