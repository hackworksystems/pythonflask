# flask関連のパッケージを取得
from flask import render_template

# 「__init__.py」で宣言した変数appを取得
from practice import app

# メニュー一覧
@app.route('/')
def menu():
  return render_template('menu.html')