# flask関連のパッケージを取得
from flask import render_template, request

# 「__init__.py」で宣言した変数appを取得
from practice import app

# 足し算
@app.route('/add')
def add():
  return render_template('add.html')

# 足し算の処理
@app.route('/calc_add', methods=["POST"])
def calc_add():
	# 送信された値を取得
  value1 = request.form.get('value1') 
  value2 = request.form.get('value2') 
	# 例外処理
  try:
    # 計算結果を返す
    sum = int(value1) + int(value2)
    return str(sum) 
  except:
    return '数値を入力してください'