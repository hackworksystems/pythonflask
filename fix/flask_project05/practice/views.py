# flask関連のパッケージを取得
from flask import render_template, request

# 「__init__.py」で宣言した変数appを取得
from practice import app

# 選択フォーム画面を表示
@app.route('/select')
def select():
  return render_template('select.html')

# 選択フォームの処理
@app.route('/result_select', methods=["POST"])
def result_select():
	# 送信された値を取得
    product = request.form.get('product')
    sites = request.form.getlist('sites')

  # 選択された商品を復元
    selected_product = ""
    if product == "ref":
      selected_product = "冷蔵庫"
    elif product == "wash":
      selected_product = "洗濯機"
    elif product == "tv":
      selected_product = "テレビ"
    else:
      selected_product = "選択されていません"


	# 選択されたサイトを復元
    selected_sites = ""
    if len(sites) != 0:
      for site in sites:
        if site == "fb":
          selected_sites += "Facebook "
        elif site == "tw":
          selected_sites += "Twitter "
        elif site == "yt":
          selected_sites += "YouTube "
        else:
          selected_sites = "??? "
    else:
      selected_sites = "選択されていません"

    return "商品：" + selected_product + ", サイト：" + selected_sites
