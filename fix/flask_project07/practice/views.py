# flask関連のパッケージを取得
from flask import render_template, request, url_for

# 「__init__.py」で宣言した変数appを取得
from practice import app

# 選択フォーム画面の表示
@app.route('/radio')
def radio():
  return render_template('radio.html')
  
# 選択フォームの処理
@app.route('/result_radio', methods=["POST"])
def result_radio():
	# 送信された値を取得
    pay = request.form.get('pay')

	# 選択された支払い方法を復元
    selected_pay = ""
    if pay == "card":
      selected_pay = "クレジットカード"
    elif pay == "bank":
      selected_pay = "銀行振り込み"
    elif pay == "cash":
      selected_pay = "代引き"
    else:
      selected_pay = "選択されていません"
   
    return render_template('result_radio.html', selected_pay=selected_pay, pay=pay)