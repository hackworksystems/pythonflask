# flask関連のオブジェクトを取得
from flask import render_template, request, url_for, session, redirect

# 「__init__.py」で宣言した変数appを取得
from practice import app

# トップ画面
@app.route('/')
def top():
  if not session.get('logged_in'):
    return redirect(url_for('login'))
  return render_template('top.html')

# ログイン
@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    if request.form.get('user_id') != app.config['USER_ID']:
      print('ユーザIDが異なります')
    elif request.form.get('password') != app.config['PASSWORD']:
      print('パスワードが異なります')
    else:
      session['logged_in'] = True
      return redirect(url_for('top'))
  return render_template('login.html')

# ログアウト
@app.route('/logout')
def logout():
  session.pop('logged_in', None)
  print('ログアウトしました')
  return redirect(url_for('login'))
