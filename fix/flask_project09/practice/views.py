# flask関連のパッケージを取得
from flask import render_template, request, url_for, session, redirect, flash

# 「__init__.py」で宣言した変数appを取得
from practice import app

# トップ画面
@app.route('/')
def top():
  if not session.get('logged_in'):
    flash('ログインしてください', 'error')
    return redirect(url_for('login'))
  return render_template('top.html')

# ログイン
@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    if request.form.get('user_id') != app.config['USER_ID']:
      flash('ユーザIDが異なります', 'error')
    elif request.form.get('password') != app.config['PASSWORD']:
      flash('パスワードが異なります', 'error')
    else:
      session['logged_in'] = True
      flash('ログインしました', 'success')
      return redirect(url_for('top'))
  return render_template('login.html')

# ログアウト
@app.route('/logout')
def logout():
  session.pop('logged_in', None)
  flash('ログアウトしました', 'success')
  return redirect(url_for('login'))