# flask関連のパッケージを取得
from flask import render_template, request, url_for, session, redirect, flash

# 「__init__.py」で宣言した変数appを取得
from practice import app

# デコレーターに使用
from functools import wraps

# ログインチェック処理
def login_check(view):
  @wraps(view)
  def inner(*args, **kwargs):
    if not session.get('logged_in'):
      flash('ログインしてください', 'error')
      return redirect(url_for('login'))    
    return view(*args, **kwargs)
  return inner

# トップ画面
@app.route('/')
@login_check
def top():
  return render_template('top.html')

# ユーザ一覧画面
@app.route('/users')
@login_check
def users():
  return render_template('users.html')

# ログイン
@app.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    if request.form.get('user_id') != app.config['USER_ID']:
      flash('ユーザIDが異なります', 'error')
    elif request.form.get('password') != app.config['PASSWORD']:
      flash('パスワードが異なります', 'error')
    else:
      session['logged_in'] = True
      flash('ログインしました', 'success')
      return redirect(url_for('top'))
  return render_template('login.html')

# ログアウト
@app.route('/logout')
def logout():
  session.pop('logged_in', None)
  flash('ログアウトしました', 'success')
  return redirect(url_for('login'))