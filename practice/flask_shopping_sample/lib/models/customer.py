﻿from flask.globals import session
from lib.db import db
from . import Order

class Customer(db.Model):

    __tablename__ = 'customers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    address = db.Column(db.String(100), nullable=False)
    tel = db.Column(db.String(20), nullable=False)
    # add emailをログイン時のIDとして使うため一意性制約(unique=True)を追加
    email = db.Column(db.String(64), nullable=False, unique=True)
    # add passwordカラムを追加
    # nullable = Falseなカラムを追加するときはdefault値を設定しないと
    # 登録済みのレコードのカラムがNULLになってしまいエラーになる
    password = db.Column(db.String(64), nullable=False, default='password')
    # add 顧客別注文一覧を取得できるよう変更
    orders = db.relationship('Order', backref='customer', uselist=True)

    # add ログインメソッドの追加
    @classmethod
    def login(self, email, password):
        """ ログイン

        Parameters
        ------
        email: str
            IDとして使用するメールアドレス
        password: str
            パスワード

        Returns
        ------
        Customer or None
            ログインに成功すればCustomer オブジェクト、失敗すればNone
        """
        return db.session.query(Customer).filter(
            Customer.email == email,
            Customer.password == password
            ).one_or_none()

    # add passwordを引数に追加
    def __init__(self, name, address, tel, email, password):
        self.name = name
        self.address = address
        self.tel = tel
        self.email = email
        # add 
        self.password = password

    # add 新規追加
    def cancel(self, order_id):
        """
        注文を削除する

        Parameters
        ------
        order_id : int
            削除する注文ID
        """
        db.session.query(Order).filter(
            Order.customer_id == self.id,
            Order.id == order_id
        ).delete()
        db.session.commit()

    # add 新規追加
    def cancel_all(self):
        """
        注文を全て削除する

        Parameters
        ------
        """
        db.session.query(Order).filter(
            Order.customer_id == self.id
        ).delete()
        db.session.commit()