# 「__init__.py」で宣言した変数appを取得
from shopping import app
from admin.views import app as admin_app

app.register_blueprint(admin_app,url_prefix='/admin')
app.config.from_object('admin.config')

# アプリケーションの起動
if __name__ == '__main__':
  app.run()
