# add 新規追加

# flask関連のパッケージを取得
from flask import render_template, request, url_for, session, redirect, flash, Blueprint

# Itemモデルを取得
from lib.models import Order, Customer, Item

# SQLAlchemyを取得
from lib.db import db

# Blueprintでitemアプリケーションを登録
customer = Blueprint('customers', __name__)

# サインアップ入力フォーム
@customer.route('/signup')
def signup():
  return render_template('customers/signup.html')

# 会員登録
@customer.route('/regist', methods=['POST'])
def regist():
  try:
    name = request.form.get('name')
    email = request.form.get('email')
    password = request.form.get('password')
    address = request.form.get('address')
    tel = request.form.get('tel')
    customer = Customer(name,address,tel,email,password)
    db.session.add(customer)
    db.session.commit()
  except:
    # TODO エラー別の対応
    flash('サインアップできませんでした', 'error')
    return redirect(url_for('customers.signup'))
  session['logged_in'] = customer.id
  flash('サインアップしました', 'success')
  return redirect(url_for('item.index'))

# ログイン
@customer.route('/login', methods=['GET', 'POST'])
def login():
  if request.method == 'POST':
    email = request.form.get('email')
    password = request.form.get('password')
    customer = Customer.login(email, password)
    if customer == None:
      flash('ユーザIDまたはパスワードが異なります', 'error')
    else:
      # add ログインしたらセッション変数に顧客IDを保存する
      session['logged_in'] = customer.id
      flash('ログインしました', 'success')
      return redirect(url_for('item.index'))

  return render_template('customers/login.html')

# ログアウト
@customer.route('/logout')
def logout():
  session.pop('logged_in', None)
  # add ログアウトしたらカートを破棄する
  session.pop('cart', None)
  flash('ログアウトしました', 'success')
  return redirect(url_for('customers.login'))
