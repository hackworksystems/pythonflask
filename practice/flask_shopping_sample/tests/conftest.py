import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from lib.db import init_db
from lib.db import db

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import pytest

@pytest.fixture(scope="session")
def context():
    app = Flask(__name__)

    DB_CONN = 'postgresql+psycopg2://{user}:{password}@{host}/flask_shopping'.format(**{
        'user': os.getenv('DB_USER', 'postgres'),
        'password': os.getenv('DB_PASSWORD', 'himitu'),
        'host': os.getenv('DB_HOST', 'localhost:5432'),
    })

    app.config['SQLALCHEMY_DATABASE_URI'] = DB_CONN
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    
    init_db(app)


    with app.app_context() as ct:
        with open(os.path.join(os.path.dirname(__file__),'init.sql'),encoding='utf-8') as f:
            sql = f.read()
            db.session.execute(sql)
        yield ct
    
