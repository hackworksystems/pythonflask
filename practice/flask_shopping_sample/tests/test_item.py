import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from lib.db import db

# Itemモデルを取得
from lib.models import Item

def test_get(context):
    item = Item.query.get(1)
    assert item.name == 'Javaの基本'

def test_add(context):
    item = Item(1, 'test', 2000)
    db.session.add(item)
    db.session.commit()
    # 追加したレコードを取得
    item2 = Item.query.get(item.id)
    assert item2.category.id == 1
    assert item2.name == 'test'
    assert item2.price == 2000